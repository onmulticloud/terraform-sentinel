###########################################################
#Secure Configuration. Configurations of Network, System or Application shall be approved and hardened according to relevant IT Security Standards.
# // Check if access logs are enabled on AWS WAF
###########################################################

import "tfplan/v2" as tfplan
import "tfconfig/v2" as tfconfig
import "strings"

#Check if classic WAF has access logs enabled
all_wafs = filter tfplan.resource_changes as _, rc {
	rc.type is "aws_wafv2_web_acl" and
		rc.mode is "managed" and
		(rc.change.actions contains "create" or rc.change.actions contains "update" or
			rc.change.actions contains "read" or
			rc.change.actions contains "no-op")
}

all_waf_list = []
for all_wafs as _, waf {
	append(all_waf_list, strings.trim_prefix(waf.address, "aws_wafv2_web_acl."))
}

# Get all logging configurations of AWS WAF
waf_access_logs_configurations = filter tfconfig.resources as _, r {
	r.mode is "managed" and
		r.type is "aws_wafv2_web_acl_logging_configuration"
}

waf_access_logs_configurations_list = []
for waf_access_logs_configurations as _, waf_access_logs {
	if length(waf_access_logs.config.resource_arn.references) != 0 {
		append(waf_access_logs_configurations_list, strings.trim_prefix(waf_access_logs.config.resource_arn.references[1], "aws_wafv2_web_acl."))
	}
}

# # Count violations
violations = 0
for all_waf_list as _, waf {
	if waf not in waf_access_logs_configurations_list {
		print("Violation: AWS WAF - " + waf +" - must have access logs enabled")
		violations = violations + 1
	}
}

main = rule {
	violations <= 0
}
