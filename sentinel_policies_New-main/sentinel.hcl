module "tfplan-functions" {
    source = "./SentinelPolicies/tfplan-functions.sentinel"
}

policy "d1_s3_2" {
   enforcement_level = "hard-mandatory"
   source = "./SentinelPolicies/d1_s3_2.sentinel"
}