



policy "ec2-tag-mandate-check" {
   enforcement_level = "hard-mandatory"
   source = "./ec2-tag-check.sentinel"
}

policy "s3-bucket-public-read-prohibited" {
   enforcement_level = "hard-mandatory"
   source = "./s3-public-access-check.sentinel"
}